export const fileAsString = (file) => new Promise((resolve,reject) => {
  const r = new FileReader();
  r.onload = () => resolve(r.result);
  r.onerror = error => reject(error);
  r.readAsDataURL(file);
});