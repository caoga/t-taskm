import React, { useEffect } from 'react';

import TaskAddModal from '../../components/TaskAddModal';
import EditModal from '../../components/EditModal';
import LoginModal from '../../components/LoginModal';
import Pagination from '../../components/Pagination';

import Button from '../../components/ui/button';
import Task from '../../components/Task';
import styled from 'styled-components';

import { useDispatch, useSelector } from 'react-redux';
import { fetchTasks, changeSorting, changePage, toggleStatus } from '../../ducks/tasks';
import { toggleLoginModal, toggleNewTaskModal } from '../../ducks/modals';

const Container = styled.div`
  font-family: 'Open Sans', sans-serif;
  width: 960px;
  margin: 0 auto;
`;

const Table = styled.table`
  margin-top: 10px;
  border-spacing: 0;
  width: 100%;
  thead {
    padding-bottom: 10px;
  }
  th {
    font-weight: 600;
    text-align: left;
    &:hover {
      cursor: pointer;
    }
  }
`;

const TwoSides = styled.div`
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-between;
  margin-top: ${props => props.margin + 'px'}
`;



const Dashboard = () => {
  const dispatch = useDispatch();
  const { logged } = useSelector(state => state.auth);
  const showAll = useSelector(state => state.tasks.showAll);
  const tasks = useSelector(state => state.tasks.list);
  const [ sortKey, sortOrder ] = useSelector(state => state.tasks.sorting);
  const page = useSelector(state => state.tasks.page);
  useEffect(() => {
    dispatch(fetchTasks());
  }, [dispatch]);

  const complement = sortOrder === 'asc' ? 1 : -1;

  const compareFn = sortKey === 'createdAt' 
    ? (a, b) => (a[sortKey] - b[sortKey]) * complement
    : (a, b) => (a[sortKey].toLowerCase().localeCompare(b[sortKey].toLowerCase())) * complement;


  const sortedTasks = tasks.filter(x => showAll || x.status === 'active').sort(compareFn).slice((page - 1) * 3, page * 3);
  return (
    <Container>
      <TaskAddModal />
      <LoginModal />
      <EditModal />
      <TwoSides>
        <Button onClick={() => dispatch(toggleNewTaskModal(true))}>New Task</Button>
        {logged ? "Logged in" : <Button onClick={() => dispatch(toggleLoginModal(true))}>Admin access</Button>}
      </TwoSides>
      <Table>
        <thead>
          <tr>
            <th onClick={() => dispatch(changeSorting('title'))}>Title</th>
            <th onClick={() => dispatch(changeSorting('authorName'))}>Author</th>
            <th onClick={() => dispatch(changeSorting('authorMail'))}>Email</th>
            <th onClick={() => dispatch(changeSorting('createdAt'))}>Created at</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {sortedTasks.map(x => <Task {...x} key={x.id} />)}
        </tbody>
      </Table>
      <TwoSides margin="30">
        {sortedTasks.length > 3 ? <Pagination /> : <div />}
        <Button onClick={() => dispatch(toggleStatus())}>{showAll ? "Show only active" : "Show all tasks"}</Button>
      </TwoSides>
    </Container>
  );
}

export default Dashboard;