import React from 'react';
import { render } from 'react-dom';

import { Provider } from 'react-redux';
import store from './store';

import Dashboard from './containers/Dashboard';

import loadFonts from './fonts';

loadFonts();

render(
  <Provider store={store}>
    <Dashboard/>
  </Provider>,
  document.getElementById('root'),
);
