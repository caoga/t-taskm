import { combineReducers } from 'redux';

import tasks from './tasks';
import modals from './modals';
import auth from './auth';

export default combineReducers ({
  tasks,
  modals,
  auth,
});