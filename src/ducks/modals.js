const MODAL_NEWTASK = 'MODAL/NEW_TASK';
const MODAL_LOGIN = 'MODAL/LOGIN';
const MODAL_EDIT = 'MODAL/EDIT';

export const toggleNewTaskModal = data => ({ type: MODAL_NEWTASK, data });
export const toggleLoginModal = data => ({ type: MODAL_LOGIN, data });
export const toggleEditModal = data => ({ type: MODAL_EDIT, data });

const initialState = ({
  newTask: false,
  login: false,
  edit: false,
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case MODAL_NEWTASK:
      return { ...state, newTask: action.data === undefined ? !state.newTask : action.data };
    case MODAL_LOGIN:
      return { ...state, login: action.data === undefined ? !state.login : action.data };
    case MODAL_EDIT:
      return { ...state, edit: action.data === undefined ? !state.edit : action.data };
    default:
      return state;
  }
};

export default reducer;