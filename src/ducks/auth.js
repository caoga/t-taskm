import { login } from "../api/auth";

const LOGIN_REQUEST = 'LOGIN/REQUEST';
const LOGIN_SUCCESS = 'LOGIN/SUCCESS';
const LOGIN_FAIL = 'LOGIN/FAIL';


const fetchLogin = () => ({ type: LOGIN_REQUEST });
const fetchLoginSuccess = data => ({ type: LOGIN_SUCCESS, data });
const fetchLoginFail = error => ({ type: LOGIN_FAIL, error });


export const attemptLogin = (credentials) => async dispatch => {
  dispatch(fetchLogin());
  try {
    const response = await login(credentials);
    dispatch(fetchLoginSuccess(response));
    return response;
  } catch (e) {
    dispatch(fetchLoginFail(e));   
    return e;
  }
}


const initialState = {
  logged: false,
  loading: false,
  error: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_REQUEST: 
      return { ...state, logged: false, loading: true };
    case LOGIN_SUCCESS:
      return { ...state, logged: true, loading: false, error: "" } ;
    case LOGIN_FAIL:
      return { ...state, logged: false, loading: false, error: action.error };
    default: 
      return state;
  }
};

export default reducer;