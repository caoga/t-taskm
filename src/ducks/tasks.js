import { getTasks, addTask as apiAddTask, editTask as apiEditTask } from "../api/tasks";

const FETCH_TASKS = 'TASKS/FETCH';
const FETCH_TASKS_SUCCESS = 'TASKS/FETCH_SUCCESS';
const FETCH_TASKS_FAIL = 'TASKS/FETCH_FAIL';

const ADD_TASK = 'TASKS/ADD';
const EDIT_TASK = 'TASK/EDIT';
const CHANGE_SORTING = 'TASKS/SORTING';
const CHANGE_PAGE = 'TASKS/PAGE';
const TOGGLE_STATUS = 'TASKS/TOGGLE_STATUS'

const SET_EDIT_ID = 'TASKS/SET_EDIT_ID'; 

const fetchTasksRequest = () => ({ type: FETCH_TASKS });
const fetchTasksSuccess = data => ({ type: FETCH_TASKS_SUCCESS, data });
const fetchTasksFail = error => ({ type: FETCH_TASKS_FAIL, error });

export const changeSorting = data => ({ type: CHANGE_SORTING, data });
export const changePage = data => ({ type: CHANGE_PAGE, data });
export const toggleStatus = () => ({ type: TOGGLE_STATUS });
export const setEditId = id => ({ type: SET_EDIT_ID, data: id });

export const fetchTasks = () => async dispatch => {
  dispatch(fetchTasksRequest());
  try {
    const tasks = await getTasks();
    dispatch(fetchTasksSuccess(tasks));
    return tasks;
  } catch (e) {
    dispatch(fetchTasksFail(e));   
  }
}

export const addTask = (data) => async dispatch => {
  const task = {
    ...data, 
    id: Math.random().toString(36).substr(2, 9),
    createdAt: new Date().valueOf(),
    status: 'active',
  };
  const result = await apiAddTask(task);
  // makes little sense in sync environment to handle result here
  dispatch({ type: ADD_TASK });
  dispatch(fetchTasks());
}

export const editTask = (id, data) => async dispatch => {
  const result = await apiEditTask(id, data);
  dispatch({ type: EDIT_TASK });
  dispatch(fetchTasks());
}

const initialState = {
  list: [],
  loading: true,
  error: false,
  sorting: ['title', 'asc'],
  page: 1,
  showAll: false,
  editId: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TASKS: 
      return { ...state, loading: true, error: false };
    case FETCH_TASKS_SUCCESS:
      return { ...state, loading: false, error: false, list: action.data };
    case FETCH_TASKS_FAIL:
      return { ...state, loading: false, error: true };
    case CHANGE_SORTING: 
      if (action.data === state.sorting[0]) {
        return { ...state, sorting: [action.data, state.sorting[1] === 'asc' ? 'desc' : 'asc'] };
      }
      return { ...state, sorting: [action.data, 'asc'] };
    case CHANGE_PAGE:
      return { ...state, page: action.data };
    case TOGGLE_STATUS:
      return { ...state, showAll: !state.showAll };
    case SET_EDIT_ID: 
      return { ...state, editId: action.data };
    default: 
      return state;
  }
};

export default reducer;