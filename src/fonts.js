import webfont from 'webfontloader';

const load = () => webfont.load({
  google: {
    families: ['Open Sans', 'sans-serif'],
  },
});

export default load;