export const getTasks = () => JSON.parse(localStorage.getItem('tasks')) || [];
const setTasks = tasks => localStorage.setItem('tasks', JSON.stringify(tasks));
export const getTask = id => getTasks().find(x => x.id === id);
export const addTask = task => setTasks([...getTasks(), task]);
export const removeTask = id => setTasks(getTasks().filter(x => x.id !== id));
export const editTask = (id, task) => {
  console.log('task being edited');
  const tasks = getTasks();
  const index = tasks.findIndex(x => x.id === id);
  tasks[index] = {...tasks[index], ...task};
  setTasks(tasks);
};