import React, { useState } from 'react';
import Modal from '../ui/modal';
import Input from '../ui/input';
import Button, { style as ButtonStyle } from '../ui/button';

import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { addTask } from '../../ducks/tasks';
import { toggleNewTaskModal } from '../../ducks/modals';
import { fileAsString } from '../../utils';

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 150px 1fr;
  grid-row-gap: 10px;
  & label {
    line-height: 34px;
  }
`;

const StyledButtons = styled.div`
  margin-top: 15px;
  display: flex;
  justify-content: space-between;
`;

const StyledDivButton = styled.div`${ButtonStyle}`;

const TaskAddModal = () => {
  const isOpen = useSelector(state => state.modals.newTask);
  const dispatch = useDispatch();

  const [fields, setFields] = useState({
    title: "",
    description: "",
    authorName: "",
    authorMail: "",
    image: "",
    imageName: "",
  });

  const handleChange = e => setFields(x => ({...x, [e.target.name]: e.target.value}));

  const handleSave = () => {
    dispatch(addTask(fields));
    dispatch(toggleNewTaskModal(false));
  };

  const handleImage = async e => {
    const file = e.target.files[0];
    const b64 = await fileAsString(file);
    setFields(x => ({...x, image: b64, imageName: file.name}));
  };

  return (
  <Modal isOpen={isOpen} onClose={() => dispatch(toggleNewTaskModal(false))}>
    <FormGrid>
      <label>Title: </label><Input name="title" onChange={handleChange} />
      <label>Author name: </label><Input name="authorName" onChange={handleChange} />
      <label>Author email: </label><Input name="authorMail" onChange={handleChange} />
      <label>Description: </label><Input name="description" onChange={handleChange} />
    </FormGrid>
    <StyledButtons>
      <div style={{display: "flex", alignItems: "center"}}>
        <label htmlFor="image"><StyledDivButton>Select Image</StyledDivButton></label>
        <input type="file" id="image" style={{display: 'none'}} onChange={handleImage}/>
        {fields.imageName}
      </div>
      <Button onClick={handleSave}>Save</Button>
    </StyledButtons>
  </Modal>
  );
}

export default TaskAddModal;