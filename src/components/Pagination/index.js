import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { changePage } from '../../ducks/tasks';
import Button from '../ui/button';


const StyledPageCoutner = styled.div`
  display: inline-block;
  margin: 0 20px;
`

const Pagination = () => {
  const { page, list } = useSelector(state => state.tasks);
  const dispatch = useDispatch();
  const maxPages = Math.ceil(list.length / 3);
  const buttonHandler = x => () => (page + x <= maxPages && page + x > 0) ? dispatch(changePage(page+x)) : null;
  return (
    <div>
      <Button onClick={buttonHandler(-1)}>Previous page</Button>
      <StyledPageCoutner>{page}/{maxPages} </StyledPageCoutner>
      <Button onClick={buttonHandler(+1)}>Next page</Button>
    </div>
  )
}

export default Pagination;