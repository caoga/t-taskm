import React from 'react';
import styled from 'styled-components';

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
`;

const ModalBody = styled.div`
  width: 40%;
  min-width: 320px;
  min-height: 100px;
  background-color: #fafafa;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  border-radius: 3px;
  padding: 30px;
`;


const Modal = ({ isOpen, onClose, children }) => {
  const noBubbling = e => e.stopPropagation();
  if (!isOpen) return null;
  return (
    <Overlay onClick={onClose}>
      <ModalBody onClick={noBubbling}>
        {children}
      </ModalBody>
    </Overlay>
  )
};

export default Modal;