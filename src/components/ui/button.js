import styled, {css} from 'styled-components';

export const style = css`
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  color: #fafafa;
  background-color: ${props => props.inactiveColor || '#43a047'};
  border: none;
  border-radius: 3px;
  font-size: 16px;
  height: 34px;
  line-height: 34px;
  padding: 0 15px;
  transition: background-color .2s ease-in-out;
  &:hover {
    background-color: ${props => props.hoverColor || '#388e3c'};
    cursor: pointer;
  }
  &:active {
    background-color: ${props => props.activeColor || '#00701a'};
  }
  &:focus {
    outline: none;
  }
`;

const Button = styled.button`
  ${style}
`;

export default Button;