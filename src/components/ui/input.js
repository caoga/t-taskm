import styled from 'styled-components';

const Input = styled.input`
  font-family: 'Open Sans', sans-serif;
  border: 1px solid #bdbdbd;
  border-radius: 3px;
  font-size: 18px;
  height: 30px;
  transition: border-color .2s ease-in-out;
  &:focus {
    border-color: #00c853;
    outline: none;
  }
`;

export default Input;