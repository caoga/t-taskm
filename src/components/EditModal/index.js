import React, { useState } from 'react';
import Modal from '../ui/modal';
import Input from '../ui/input';
import Button, { style as ButtonStyle } from '../ui/button';

import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { editTask } from '../../ducks/tasks';
import { toggleEditModal } from '../../ducks/modals';
import { fileAsString } from '../../utils';

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 150px 1fr;
  grid-row-gap: 10px;
  & label {
    line-height: 34px;
  }
`;

const StyledButtons = styled.div`
  margin-top: 15px;
  display: flex;
  justify-content: space-between;
`;

const StyledDivButton = styled.div`${ButtonStyle}`;



const TaskEditModal = () => {
  const isOpen = useSelector(state => state.modals.edit);
  const dispatch = useDispatch();
  const { list, editId } = useSelector(state => state.tasks);
  const focus = list.find(x => x.id === editId) || {};
  console.log(focus.description, focus.status);
  const [fields, setFields] = useState({
    description: "",
    status:  "",
    image: "",
    imageName: "",
  });
  console.log(fields);
  const handleChange = e => setFields(x => ({...x, [e.target.name]: e.target.value}));

  const handleSave = () => {
    const cleanFields = Object.fromEntries(Object.entries(fields).filter(([key, value]) => Boolean(value)));
    dispatch(editTask(editId, cleanFields));
    dispatch(toggleEditModal(false));
  };

  const handleImage = async e => {
    const file = e.target.files[0];
    const b64 = await fileAsString(file);
    setFields(x => ({...x, image: b64, imageName: file.name}));
  };

  return (
  <Modal isOpen={isOpen} onClose={() => dispatch(toggleEditModal(false))}>
    <FormGrid>
      <label>Description: </label><Input name="description" defaultValue={focus.description} onChange={handleChange} />
      <label>Status: </label><Input name="status" defaultValue={focus.status} onChange={handleChange} />
    </FormGrid>
    <StyledButtons>
      <div style={{display: "flex", alignItems: "center"}}>
        <label htmlFor="image"><StyledDivButton>Change image</StyledDivButton></label>
        <input type="file" id="image" style={{display: 'none'}} onChange={handleImage}/>
        {fields.imageName}
      </div>
      <Button onClick={handleSave}>Save</Button>
    </StyledButtons>
  </Modal>
  );
}

export default TaskEditModal;