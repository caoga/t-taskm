import React, { useState } from 'react';
import Modal from '../ui/modal';
import Input from '../ui/input';
import Button from '../ui/button';
import { useDispatch, useSelector } from 'react-redux';
import { toggleLoginModal } from '../../ducks/modals';
import styled from 'styled-components';
import { attemptLogin } from '../../ducks/auth';

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 150px 1fr;
  grid-row-gap: 10px;
  & label {
    line-height: 34px;
  }
`;

const StyledError = styled.div`
  color: #f44336;
  line-height: 34px;
  margin-left: 30px;
`


const LoginModal = () => {
  const isOpen = useSelector(state => state.modals.login);
  const { error } = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const [fields, setFields] = useState({
    login: "",
    password: "",
  });
  const handleChange = e => setFields(x => ({...x, [e.target.name]: e.target.value}));
  const handleLogin = async e => {
    e.preventDefault();
    const { status } = await dispatch(attemptLogin(fields));
    if (status === 200) {
      dispatch(toggleLoginModal(false));
    }
  };
  return (
    <Modal isOpen={isOpen} onClose={() => dispatch(toggleLoginModal(false))}>
      <FormGrid>
        <label>Login: </label><Input name="login" onChange={handleChange} />
        <label>Password: </label><Input name="password" type="password" onChange={handleChange} />
        <Button onClick={handleLogin}>Log in</Button><StyledError>{error && error.message}</StyledError>
      </FormGrid>
    </Modal>
  )
};

export default LoginModal;