import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { toggleEditModal } from '../../ducks/modals';
import { setEditId } from '../../ducks/tasks';

const Indicator = styled.div`
  width: 20px;
  height: 20px;
  border: 1px solid #0e0e0e;
  border-radius: 50%;
  display: inline-flex;
  vertical-align: top;
  margin-right: 5px;
  align-items: center;
  justify-content: center;
  transform: ${props => props.rotated ? 'rotate(90deg)' : 'none'};
  transition: transform .2s ease-in-out;
  &:hover {
    cursor: pointer;
  }
`

const Triangle = styled.div`
  width: 0;
  height: 0;
  border-top: 5px solid transparent;
  border-bottom: 5px solid transparent;
  border-left: 5px solid #0e0e0e;
`;

const Image = styled.img`
  display: block;
  max-width: 320px;
  max-height: 240px;
  width: auto;
  height: auto;
`;

const EditCell = styled.td`
  display: flex;
  justify-content: space-between;
`;

const EditSpan = styled.span`
  color: #0288d1;
  padding-right: 20px;
  cursor: pointer;
`;

const Task = ({
  id, title, description,
  authorName, authorMail, createdAt, status,
  image,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const { logged } = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const toggle = () => setIsOpen(state => !state);

  const handleEdit = e => {
    e.preventDefault();
    if (!logged) return false;
    dispatch(toggleEditModal(true));
    dispatch(setEditId(id));
  }
  return (
    <>
      <tr>
        <EditCell>
          <span>
            <Indicator rotated={isOpen} onClick={toggle}><Triangle /></Indicator>
            {title}
          </span>
          {logged ? <EditSpan onClick={handleEdit}>edit</EditSpan> : null}
        </EditCell>
        <td>{authorName}</td>
        <td>{authorMail}</td>
        <td>{new Date(createdAt).toLocaleString()}</td>
        <td>{status}</td>
      </tr>
      {isOpen && 
      <tr>
        <td colSpan="5">
          <Image src={image} />
          {description}
        </td>
      </tr>
      }
    </>
  )
};

export default Task;